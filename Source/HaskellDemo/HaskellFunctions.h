// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "HaskellFunctions.generated.h"

/**
 * 
 */
UCLASS()
class HASKELLDEMO_API UHaskellFunctions : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable, Category = "Haskell")
	static bool initHaskell(FString folder, FString name);
	
	UFUNCTION(BlueprintCallable, Category = "Haskell")
	static void deinitHaskell();

	UFUNCTION(BlueprintCallable, Category = "Haskell")
	static FString haskellHello(FString name);
};
