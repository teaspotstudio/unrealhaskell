// Fill out your copyright notice in the Description page of Project Settings.

#include "HaskellDemo.h"
#include "HaskellFunctions.h"

typedef char*(*_haskellHello)(char* name);

_haskellHello m_haskellHello;

void *dllHandle;

static bool loadSymbols();

#pragma region Load DLL
bool UHaskellFunctions::initHaskell(FString folder, FString name) 
{
#if PLATFORM_WINDOWS
	FString prefix = "";
	FString postfix = "dll";
#elif PLATFORM_LINUX
	FString prefix = "lib";
	FString postfix = "so";
#else 
	#error "Unsupported platform!"
#endif 

	deinitHaskell();

	FString filePath = FPaths::GamePluginsDir() + "/" + folder + "/" + prefix + name + "." + postfix;

	if (FPaths::FileExists(filePath))
	{
		dllHandle = FPlatformProcess::GetDllHandle(*filePath);
		if (dllHandle != NULL)
		{
			return loadSymbols();
		}
	}

	return false;
}

bool loadSymbols()
{
	FString procName = "haskellHello";
	m_haskellHello = (_haskellHello)FPlatformProcess::GetDllExport(dllHandle, *procName);
	if (m_haskellHello == NULL) return false;

	return true;
}

void UHaskellFunctions::deinitHaskell()
{
	if (dllHandle != NULL)
	{
		m_haskellHello = NULL;

		FPlatformProcess::FreeDllHandle(dllHandle);
		dllHandle = NULL;
	}
}
#pragma endregion Load DLL

FString UHaskellFunctions::haskellHello(FString name)
{
	if (m_haskellHello != NULL)
	{
		char* namec = TCHAR_TO_UTF8(*name);
		char* ret = m_haskellHello(namec);
		return UTF8_TO_TCHAR(ret);
	}

	return "Error: haskellHello is not loaded!";
}

