module Export() where

import Foreign.C

foreign export ccall "haskellHello" c_haskellHello :: CString -> IO CString

c_haskellHello :: CString -> IO CString
c_haskellHello cstr = do
  str <- peekCString cstr
  newCString $ "Hello, " ++ str ++ "!"
